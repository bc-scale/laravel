<?php

namespace App\Console\Commands\NovaPoshta;

use App\Services\NovaPoshta\NovaposhtaService;
use Illuminate\Console\Command;

class GetWarehouse extends Command
{

    protected $signature = 'novaposhta:getWarehouses';

    protected $description = 'Получение всех отделений с Новой Почты';

    public function handle(NovaposhtaService $novaposhtaService)
    {
        $warehouses = json_decode($novaposhtaService->getWarehouses(), true);
        if ($warehouses['success']) $novaposhtaService->saveWarehouses($warehouses);
    }

}
