<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Requests\Managers;

use App\Models\Managers;
use Illuminate\Foundation\Http\FormRequest;

class CreateManagerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'       => 'required|string|email|max:255|unique:managers',
            'password'    => ['required', 'string', 'regex:' . Managers::PASSWORD_REGEX],
            'permission'  => 'required|array',
            'name'        => 'required|string',
            'description' => 'required|string',
        ];
    }
}
