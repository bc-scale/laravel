<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed page
 * @property mixed name
 * @property mixed description
 * @property mixed position
 * @property mixed visible
 * @property mixed updated_at
 * @property mixed created_at
 */
class BannerResource extends JsonResource
{
    /**
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request) : array
    {

        return [
            'id'          => $this->id,
            'page'        => $this->page,
            'name'        => $this->name,
            "description" => $this->description,
            'position'    => $this->position,
            'visible'     => $this->visible,
            'updated_at'  => Carbon::parse($this->updated_at)->format("Y-m-d H:i:s"),
            'created_at'  => Carbon::parse($this->created_at)->format("Y-m-d H:i:s"),
        ];
    }
}
