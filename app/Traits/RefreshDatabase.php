<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Traits;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase as Base;

trait RefreshDatabase
{
    use Base;

    public function RefreshDatabase()
    {
        $this->artisan('migrate:fresh --database sqlite_memory --seed');
        $this->app[Kernel::class]->setArtisan(null);
    }

}
