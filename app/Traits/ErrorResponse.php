<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Traits;

use Str;

trait ErrorResponse
{
    protected function errorResponse($errors = null): array
    {
        $errors = $this->formatErrors($errors);

        return  ['error' => $errors];
    }

    private function formatErrors($errors)
    {
        if (is_string($errors)) {
            return $this->formatMessage($errors);
        }

        if (is_array($errors)) {
            foreach ($errors as $key => $error) {
                if (is_array($error)) {
                    $errors[$key] = $this->formatErrors($error);
                } else {
                    $errors[$key] = $this->formatMessage($error);
                }
            }
            return $errors;
        }

        return $errors;
    }

    private function formatMessage(string $message): string
    {
        $message = str_replace('.', '', $message);

        return Str::snake(strtolower($message));
    }
}
