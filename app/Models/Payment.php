<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

class Payment extends BaseModel
{
    protected $fillable = [
        'id',
        'service',
        'name',
        'description',
        'currency_id',
        'settings',
        'image',
        'enabled',
        'position',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        //
    ];
}
