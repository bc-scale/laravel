<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidTrait;

/**
 * @method static findOrFail($id)
 * @method static paginate(mixed $per_page)
 */

class Banner extends Model
{
    use HasFactory, UuidTrait;

    protected $fillable = [
        'id',
        'page',
        'name',
        'description',
        'position',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'page',
        'name',
        'description',
        'position',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function scopeDesc($query)
    {
        return $query->orderByDesc('updated_at');
    }
}
