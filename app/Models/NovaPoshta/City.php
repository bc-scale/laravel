<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models\NovaPoshta;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * @method static findOrFail(string $id)
 */
class City extends Model
{
    use HasFactory, UuidTrait;

    protected $table = 'np_cities';

    protected $fillable = [
        'id',
        'description',
        'ref',
        'area',
        'area_description',
        'area_type',
        'created_at',
        'updated_at',
        'offices',
    ];

    protected $visible = [
        'id',
        'description',
        'ref',
        'area',
        'area_description',
        'area_type',
        'created_at',
        'updated_at',
        'offices',
    ];

    protected $hidden = [
        // ничего не скрываем, у нас все честно!
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $appends = [
        'offices',
    ];

    public static function getName(string $name)
    {
        return City::where('description', $name)->get();
    }

    public function getOfficesAttribute()
    {
        return Cache::remember($this->getAttribute('ref'), config('cache.lifetime'), function () {
            return Warehouse::city($this->getAttribute('ref'))->toArray();
        });

    }

}
