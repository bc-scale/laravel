<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('delivery_id')->nullable();
            $table->decimal('delivery_price', 10, 2)->default('0.00');
            $table->uuid('payment_id')->nullable();
            $table->integer('paid')->default('0');
            $table->timestamp('date_paid')->nullable();
            $table->uuid('user_id')->nullable();
            $table->uuid('status_id')->nullable();
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('np_office')->nullable();
            $table->string('ttn')->nullable();
            $table->string('comment')->nullable();
            $table->string('ip')->nullable();
            $table->string('url')->nullable();
            $table->text('details')->nullable();
            $table->decimal('total_price', 10, 2)->default('0.00');
            $table->decimal('discount', 10, 2)->default('0.00');
            $table->decimal('coupon_discount', 10, 2)->default('0.00');
            $table->string('coupon_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
