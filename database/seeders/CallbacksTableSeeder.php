<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class CallbacksTableSeeder extends Seeder
{
    public function run()
    {
        $callbacks = [];
        $faker     = Faker::create('en_EN');

        for ($i = 1; $i <= 50; $i++) {
            $callbacks[] = [
                'id'          => $faker->uuid,
                'name'        => $faker->name,
                'phone'       => $faker->phoneNumber,
                'message'     => $faker->text($maxNbChars = 200),
                'url'         => $faker->url,
                'processed'   => Arr::random([true, false]),
                'admin_notes' => $faker->text($maxNbChars = 500),
                'updated_at'  => now()->addMinutes($i),
                'created_at'  => now()->addMinutes($i),
            ];
        }

        DB::table('callbacks')->insert($callbacks);
    }
}

