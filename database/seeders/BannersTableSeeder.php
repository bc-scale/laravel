<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class BannersTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create('ru_RU');

        $banners = [
            [
                'id'          => $faker->uuid,
                'page'        => 'home',
                'name'        => 'Главная страница',
                'description' => 'Баннер в шапке на главной странице',
                'position'    => '1',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'blog',
                'name'        => 'Страница блога',
                'description' => 'Баннер в шапке на странице блога',
                'position'    => '2',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'news',
                'name'        => 'Страница новостей',
                'description' => 'Баннер в шапке на странице новостей',
                'position'    => '3',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'category',
                'name'        => 'Страница категории',
                'description' => 'Баннер в шапке на странице категории продуктов',
                'position'    => '4',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
            [
                'id'          => $faker->uuid,
                'page'        => 'menu_blog',
                'name'        => 'Боковое меню блога',
                'description' => 'Баннер в боковом меню на странице блога',
                'position'    => '5',
                'visible'     => true,
                'updated_at'  => now(),
                'created_at'  => now(),
            ],
        ];

        DB::table('banners')->insert($banners);

    }
}

