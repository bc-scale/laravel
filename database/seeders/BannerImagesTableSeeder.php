<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;


use App\Models\Banner;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class BannerImagesTableSeeder extends Seeder
{
    public function run()
    {
        $banner_images = [];
        $faker         = Faker::create('ru_RU');

        $banners = Banner::all()->pluck('id')->toArray();

        for ($i = 1; $i <= 50; $i++) {
            $banner_images[] = [
                'id'          => $faker->uuid,
                'banner_id'   => Arr::random($banners),
                'name'        => $faker->company,
                'alt'         => $faker->text($maxNbChars = 80),
                'title'       => $faker->text($maxNbChars = 50),
                'description' => $faker->text($maxNbChars = 200),
                'link'        => $faker->url,
                'image'       => $faker->imageUrl($width = 800, $height = 600),
                'position'    => $i,
                'visible'     => true,
                'updated_at'  => now()->addMinutes($i),
                'created_at'  => now()->addMinutes($i),
            ];
        }

        DB::table('banner_image')->insert($banner_images);

    }
}

